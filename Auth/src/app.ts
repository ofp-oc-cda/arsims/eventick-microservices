import express from "express";
import "express-async-errors";
import {json} from "body-parser";
import cors from "cors";
import cookieSession from "cookie-session";
//middleware
import {errorHandlers} from "./middlewares";
import {NotFoundError} from "./errors";
import {currentUserRouter, signInRouter, signoutRouter, signupRouter,} from "./routes";

const app = express();
app.set('trust proxy', true)
app.use(json());
app.use(cors());
app.use(cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
}))

app.use(currentUserRouter);
app.use(signInRouter);
app.use(signoutRouter);
app.use(signupRouter);

app.all("*", async () => {
    throw new NotFoundError();
});

//error handler
app.use(errorHandlers);

export {app};
