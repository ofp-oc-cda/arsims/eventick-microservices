import {CustomError} from "../interfaces";

export class NotAuthorizedError extends CustomError {
    statusCode = 401;

    constructor() {
        super('Not authorized');
        Object.setPrototypeOf(this, CustomError.prototype)
    }

    serializeErrors(): [{ message: string }] {
        return [{message: 'Not authorized'}]
    }
}
