import {PasswordLength} from "../interfaces"

export const PassLength: PasswordLength = {
    min: 4,
    max: 20
}
