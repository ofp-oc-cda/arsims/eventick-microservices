import {NextFunction, Request, Response} from "express";
import jwt from "jsonwebtoken";
import {UserPayload} from "../interfaces";

declare module "express-serve-static-core" {
  export interface Request {
    currentUser?: UserPayload;
  }
}

export const currentUserMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.session?.jwt) {
    return next();
  }

  try {
    req.currentUser = jwt.verify(req.session.jwt, process.env.JWT_KEY!) as UserPayload;
    console.log("hej")
  } catch (err) {}

  next();
};
