import mongoose from "mongoose";
import {DatabaseValidationError} from "./errors";
import {app} from "./app"

const start = async () => {
    const PORT = process.env.PORT || 3000;
    if (!process.env.JWT_KEY) {
        throw new Error('JWT_KEY must be defined')
    }
    try {
        await mongoose.connect("mongodb://auth-mongo-srv:27017/auth");
        console.log("Connected to MongoDB ... ")
    } catch (error) {
        throw new DatabaseValidationError().reason;
    }

    app.listen(PORT, () => {
        console.log(`[Auth Server] - Listening on port ${PORT}`);
    });
};

start();
