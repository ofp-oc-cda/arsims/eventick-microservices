import express, {Request, Response} from 'express';
import "express-async-errors";

const router = express.Router();

router.post('/api/users/signout', async (req: Request, res: Response) => {
    req.session = null
    res.send({})
})

export {router as signoutRouter};
