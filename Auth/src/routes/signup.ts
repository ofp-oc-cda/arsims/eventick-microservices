import express, {Request, Response} from "express";
import "express-async-errors";
import jwt from "jsonwebtoken";

import {BadRequestError} from "../errors";
import {User} from "../models/UserModel";
import {ExpressValidatorSignUp} from "../constants";
import {ValidateRequest} from "../middlewares";

const router = express.Router();

router.post(
    "/api/users/signup",
    ExpressValidatorSignUp,
    ValidateRequest,
    async (req: Request, res: Response) => {
        const {username, email, password} = req.body;
        const existingUser = await User.findOne({email});

        if (existingUser?.email === email) {
            throw new BadRequestError(`${email} is in use`);
        }

        const user = User.build({username, email, password});
        await user.save();

        // Generate JWT
        const userJWT = jwt.sign(
            {
                id: user.id,
                email: user.email,
                username: user.username,
            },
            process.env.JWT_KEY!
        );

        // Store it on session object
        req.session = {
            jwt: userJWT,
        };

        res.status(201).send(user);
    }
);

export {router as signupRouter};
