import {Request, Response, Router} from "express";
import {currentUserMiddleware} from "../middlewares";

const router = Router();

router.get(
    "/api/users/currentuser",
    currentUserMiddleware,
    (req: Request<{ 'currentUser': string }>, res: Response) => {
        res.send({currentUser: JSON.stringify(req.currentUser) || null});
    }
);

export {router as currentUserRouter};
