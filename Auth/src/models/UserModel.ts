import { Schema, model } from "mongoose";
import { IUser, IUserModel, IUserDoc } from "../interfaces";
import { PasswordHash } from "../services";

// Create a Schema corresponding to the document interface
const UserSchema = new Schema<IUser>({
    username: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
      },
      versionKey: false,
    },
  }
);

// Pre save the password hashed
UserSchema.pre("save", async function (done) {
  if (this.isModified("password")) {
    const hashed = await PasswordHash.toHash(this.get("password"));
    this.set("password", hashed);
  }
  done();
});

// Create a Builder function corresponding to the document interface
UserSchema.statics.build = (attrs: IUser) => {
  return new User(attrs);
};

// Create a Model
const User = model<IUserDoc, IUserModel>("User", UserSchema);

// Export the Schema
export { User };
